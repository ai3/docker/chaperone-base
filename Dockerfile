FROM debian:buster-slim

COPY build.sh /tmp/build.sh
COPY python37_compat.patch /tmp/python37_compat.patch
COPY chaperone.conf /etc/chaperone.d/chaperone.conf
COPY deb_autistici_org.gpg /usr/share/keyrings/deb.autistici.org.gpg

RUN /tmp/build.sh && rm /tmp/build.sh /tmp/python37_compat.patch

ENTRYPOINT ["/usr/local/bin/chaperone", "--no-syslog", "--debug"]


docker-chaperone-base
===

Docker base image with [Chaperone](https://github.com/garywiz/chaperone),
a minimal init daemon for container images.

Users of this base image should provide their own chaperone configuration
files and install them in */etc/chaperone.d*.

Child images shouldn't override the container's ENTRYPOINT, which is already set
to start chaperone with the appropriate options.

Since this image is used by most of our containers, it also sets up
the ai3 Debian package repository, so we don't have to add another
dependency for this very common task, and it disables chaperone's own
/dev/log socket, as we are bind-mounting the system one.

